// Description:
//  Find the en-us name of a hex color (#ff0000)
//  Specific library search is optional
//  Color libraries: crayola, pantone, faber-castell
//
// Example: hubot, what color is #0099ff
//  Example: hubot, what pantone color is #0099ff
//
// Configuration:
//  None
//
// Commands:
//  hubot what color is {{hex}}
//  hubot what {{library}} color is {{hex}}

let calling = false
let color_data = null
const responses = ['Hey. That\'s', 'That color is', 'That\'s', 'That is', 'This color\'s', 'I think that\'s', 'That\'s definitely']
const defaultError = 'Oops! Something went wrong.'

const getData = (robot, color, library) => new Promise((resolve, reject) => {
  // One at a time
  if (calling)
    reject()

  calling = true

  // Remove #
  if (color.substr(0,1) == '#') 
    color = color.substr(1)

  // Validate hex length, 3 or 6 characters only
  if (color.length === 3) {
    color = `${color.charAt(0)}${color.charAt(0)}${color.charAt(1)}${color.charAt(1)}${color.charAt(2)}${color.charAt(2)}`
  }

  if (color.length !== 6) {
    calling = false
    reject('Bad input')
  }

  const libraryString = !!library ? `/${library.toString()}` : ''
  const url = `http://api.what-color.info/${color.toString()}${libraryString}`

  robot.http(url).get()((error, response, body) => {
    calling = false
    if (error || !body || response.statusCode !== 200) {
      reject(error || defaultError)
    }

    const color_data = JSON.parse(body)
    
    // Ok, send first result
    resolve(color_data[0], library)
  })
})

module.exports = (robot) => {

  // "hear" something said in chat
  robot.hear(/what color is (this|that)/i, (msg) => {
    msg.send('Ask me! Say, `hubot what color is #ff0000`')
  })

  // "respond" to a direct question, what color is {{hex}}?
  robot.respond(/what color is #?([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})/i, (msg) => {
    // Get data
    getData(robot, msg.match[1], null)
      .then((color_data, library) => {
        const intro = responses[Math.floor(Math.random() * responses.length)]

        msg.send(`${intro} ${color_data.color_name}`)
      })
      .catch((error) => {
        msg.send(error || defaultError)
      })
  })

  // "respond" to a direct question, what color {{library}} is {{hex}}?
  robot.respond(/what ([a-zA-Z_\-]{3,}) color is #?([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})/i, (msg) => {
    // Get data
    getData(msg, msg.match[2], msg.match[1])
      .then((color_data, library) => {
        const intro = responses[Math.floor(Math.random() * responses.length)]
        const libraryString = !!library ? `(${library} color library)` : ''

        msg.send(`${intro} ${color_data.color_name} ${libraryString}`)
      })
      .catch((error) => {
        msg.send(error || defaultError)
      })
  })
}
