# What-color-hubot

A hubot script for interacting with the what-color api.

Ask hubot to name a color for you.

You can optionally search within a specific color database like crayola or pantone.

## Commands (examples)
`what color is #0099ff`

`what crayola color is #0099ff`

## Libraries available
- crayola
- pantone
- faber-castell
- njit
- ntc 

### Installation
Please refer to the [hubot script installation guide](https://hubot.github.com/docs/)